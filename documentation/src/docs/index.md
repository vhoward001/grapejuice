title: Documentation Home
---

## 👋 Welcome

Welcome to the Grapejuice wiki. This is the repository where all the documentation for the project is kept.

### ⚠ Important notices ⚠

- Make sure to install Wine!
- Grapejuice is a **management application**! Packages will in most cases not automatically install Wine for you, since
  package management in some distributions can be extremely odd and limit your choices in custom builds.
- You need to have Wine 6.11 or above, or Wine Staging 6.16 or above for the Roblox Player to work!

Distributions mark a lot of Wine's dependencies as 'optional'. In reality, most of them aren't. If you're having issues
with Roblox not functioning properly, make sure you have all the required 'optional' dependencies installed.

## 🚀 Installing Grapejuice

Packages are available for some distributions. If a package is available for your distribution,
it's recommended that you use the package rather than installing from source. Otherwise, you'll need to install
Grapejuice from source.

Follow the installation guide that's appropriate for your distribution. Guides:

- Debian: [Install from package](Installing-from-package/Debian-10-and-similar) or
  [install from source](Installing-from-source/Debian-10-and-similar)
- Ubuntu
  - Ubuntu 18.04 (Bionic Beaver): [Install from package](Installing-from-package/Ubuntu-18.04-and-similar) or
    [install from source](Installing-from-source/Ubuntu-18.04-and-similar)
  - Ubuntu 20.04 (Focal Fossa) and above: [Install from package](Installing-from-package/Debian-10-and-similar) or
    [install from source](Installing-from-source/Debian-10-and-similar)
- Zorin OS
  - Zorin OS 15.2: [Install from package](Installing-from-package/Ubuntu-18.04-and-similar) or
    [install from source](Installing-from-source/Ubuntu-18.04-and-similar)
  - Zorin OS 16: [Install from package](Installing-from-package/Debian-10-and-similar) or
    [install from source](Installing-from-source/Debian-10-and-similar)
- Linux Mint
  - Linux Mint 20 (Ulyana): [Install from package](Installing-from-package/Debian-10-and-similar) or
    [install from source](Installing-from-source/Debian-10-and-similar)
  - Linux Mint 19.3 (Tricia): [Install from package](Installing-from-package/Ubuntu-18.04-and-similar) or
    [install from source](Installing-from-source/Ubuntu-18.04-and-similar)
  - LMDE4 (Debbie): [Install from package](Installing-from-package/Debian-10-and-similar) or
    [install from source](Installing-from-source/Debian-10-and-similar)
- Arch Linux: [Install from package](Installing-from-package/Arch-Linux-and-similar) or
  [install from source](Installing-from-source/Arch-Linux-and-similar)
- Manjaro Linux: [Install from package](Installing-from-package/Arch-Linux-and-similar) or
  [install from source](Installing-from-source/Arch-Linux-and-similar)
- NixOS: [Install from package](Installing-from-package/NixOS)
- Solus: [Install from source](Installing-from-source/Solus)
- Fedora Workstation: [Install from source](Installing-from-source/Fedora-Workstation)
- OpenSUSE: [Install from source](Installing-from-source/OpenSUSE)

**Please note that the following distributions are NOT supported:**

- Ubuntu 16.04 and older
- Linux Mint 18.x and older
- Kali Linux
- Parrot
- BlackArch
- Endless OS

## 💥 Troubleshooting

The [troubleshooting page](Troubleshooting) lists known issues with workarounds
